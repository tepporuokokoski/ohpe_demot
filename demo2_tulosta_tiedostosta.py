def etsi_ja_tulosta_tiedostosta():
    """Tiedostossa on kokonaislukuja, jotka ovat erotettuina toisistaan puolipisteellä
    (luvut voivat jakaantua myös eri riville) ja yhdellä rivillä on yksi tai useampi luku.
    Tämä funktio etsii ja tulostaa tiedostosta parilliset ja parittomat luvut
    sekä luvut, jotka alkavat numerolla 9"""

    with open("tiedostonimi.txt") as tiedosto:
        koko_sisalto = tiedosto.read()
        luvut_tiedostossa = koko_sisalto.split(";")
        parilliset = []
        parittomat = []
        nro9_ensimmaisena = []
        for luku in luvut_tiedostossa:
            if int(luku) % 2 == 0:
                parilliset.append(luku)
                if str(luku)[0] == "9":
                    nro9_ensimmaisena.append(luku)
            elif int(luku) % 2 != 0:
                parittomat.append(luku)
                if str(luku)[0] == "9":
                    nro9_ensimmaisena.append(luku)
        
        print(f"Parillisten lukujen määrä tiedostossa on: {len(parilliset)} kpl.")
        print(f"Parittomien lukujen määrä tiedostossa on: {len(parittomat)} kpl.")
        print(f"Numerolla yhdeksän alkavien lukujen määrä tiedostossa on: {len(nro9_ensimmaisena)} kpl.")