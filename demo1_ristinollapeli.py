def ristinollapeli_tilanne(matriisi: list): # matriisina tässä 3*3 matriisi 
    """pelin voittanut, jos peräjälkeen (jonona) kolme samaa vaakasuunnassa,
     pystysuunnassa tai vinottain. 
     pelaaja1 merk. 1, pelaaja2 merk. 2 ja jos tyhjä ruutu niin merk. 0"""
    eivoittoa = []
    
    for i in range(3):
            # vaakasuunta ja pystysuunta
            if matriisi[i][0] == 1 and matriisi[i][1] == 1 and matriisi[i][2] == 1:
                 return 1 # pelaaja1 voitti pelin vaakasuunnassa
            elif matriisi[i][0] == 2 and matriisi[i][1] == 2 and matriisi[i][2] == 2:
                 return 2 # pelaaja2 voitti pelin vaakasuunnassa
            elif matriisi[0][i] == 1 and matriisi[1][i] == 1 and matriisi[2][i] == 1:
                 return 1 # pelaaja1 voitti pelin pystysuunnassa
            elif matriisi[0][i] == 2 and matriisi[1][i] == 2 and matriisi[2][i] == 2:
                 return 2 # pelaaja2 voitti pelin pystysuunnassa
            else: # kumpikaan ei ole vaakasuunnassa tai pystysuunnassa (vielä) voittanut
                 eivoittoa.append(0) # tällöin listalle kertyy yhteensä 6 nollaa (rivejä 3 ja sarakkeita 3)
    # vinottain
    if (matriisi[0][0] == 1 and matriisi[1][1] == 1 and matriisi[2][2] == 1 or
            matriisi[2][0] == 1 and matriisi[1][1] == 1 and matriisi[0][2] == 1):
         return 1 # pelaaja1 voitti pelin vinottain
    elif (matriisi[0][0] == 2 and matriisi[1][1] == 2 and matriisi[2][2] == 2 or
            matriisi[2][0] == 2 and matriisi[1][1] == 2 and matriisi[0][2] == 2):
         return 2 # pelaaja2 voitti pelin vinottain
    else: # kumpikaan ei ole vinoittain (vielä) voittanut
        eivoittoa.append([0, 0]) # tällöin listalle 2 nollaa (2 mahdollista ristikkäin)

    # jos kumpikaan ei ole (vielä) voittanut
    if len(eivoittoa) == 8: # ei voittoa vaakasunnassa, pystysuunnassa eikä vinottain
         return 0 
         
